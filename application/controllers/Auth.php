<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    private $header = 'partils/header';
    private $footer = 'partils/footer';
    public $dados = array();
    

    public function __construct()
    {

        parent::__construct();
        $this->load->model('usuarios_model');
        $this->load->helper('form'); 

    }

	public function index()
	{
        $this->load->view($this->header);
        $this->load->view('login/login.php');
        $this->load->view($this->footer);
    }//index

    public function registrar()
	{
        //validação de formulario
        $this->load->library("form_validation");
        $this->form_validation->set_rules("nome", "nome", "required");
        $this->form_validation->set_rules("email", "email", "required|valid_email|is_unique[usuarios.email]");
        $this->form_validation->set_rules("senha", "senha", "required");

        if($this->form_validation->run()){
            $usuario = array(
                'nome' => $this->input->post('nome'),
                'email' => $this->input->post('email'),
                'senha' => md5($this->input->post('senha')),
            );

            if($this->usuarios_model->salvarUsuario($usuario)){
                redirectSuccess('auth', "Cadastro realizado com sucesso!");
                //echo "Usuario inserido com sucesso";
            }else{
                redirectError('auth', "Cadastro realizado com sucesso!");
                //echo "Falha ao inserir usuario";
            }
        }else{
            $this->load->view($this->header);
            $this->load->view('login/register.php');
            $this->load->view($this->footer);
        }
        
    }//registrar

    public function autenticar(){
        $email = $this->input->post('email');
        $senha = md5($this->input->post('senha'));

        $usuario = $this->usuarios_model->buscarUsuario($email, $senha);

        if($usuario){
            $this->session->set_userdata("user", $usuario);
            redirectSuccess('produtos', 'Seja bem vindo(a)!');
        }else{
            redirectError('auth', "Login ou senha inválidos." );
        }

    }//autenticar


    public function logout() {
        $this->session->unset_userdata('user');
        redirectSuccess('auth', 'Logout realizado com sucesso');
        $this->session->set_flashdata("message", "Logout realizado com sucesso");
        redirect('auth');
    }



}
