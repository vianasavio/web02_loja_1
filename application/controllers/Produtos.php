<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produtos extends CI_Controller {

    private $header = 'partils/header';
    private $footer = 'partils/footer';
    public $data = array();


    public function __construct() {
        parent::__construct();

        //echo($this->session->userdata('user')->id);
        
        if(!$this->session->userdata('user')) redirect('auth');

        $this->load->model('produtos_model');
        $this->load->model('usuarios_model'); 
        $this->load->helper('currency_helper');
        $this->load->helper('form'); 
    }

	public function index()
	{
        $this->data['produtos'] = $this->produtos_model->buscaProdutos();

        $this->load->view($this->header);
        $this->load->view('produtos/produtos_listar', $this->data);
        $this->load->view($this->footer);
        
    }

    public function cadastrar(){

        //validação de formulario
        $this->load->library("form_validation");
        $this->form_validation->set_rules("nome", "nome", "required");
        $this->form_validation->set_rules("descricao", "descricao", "required");
        $this->form_validation->set_rules("preco", "preco", "required");
        $sucesso = $this->form_validation->run();

        if($sucesso){
            $value['nome'] = $this->input->post('nome');
            $value['descricao'] = $this->input->post('descricao');
            $value['valor'] = $this->input->post('descricao');
            $value['valor'] = $this->session->userdata('user')->id;
            if($this->produtos_model->insert($values)){
                redirectSuccess('produtos', 'Produto cadastrado com sucesso!');
            }else{
                redirectError('produtos', 'Falha ao cadastrar Produto!');
            }
        }else{
            $this->load->view($this->header);
            $this->load->view('produtos/produtos_cadastrar');
            $this->load->view($this->footer);
        }
    }

    public function visualizar($id){

        $this->data['produto'] = $this->produtos_model->getProduto($id);
        $this->data['user'] = $this->usuarios_model->getUser($this->data['produto']->usuario_id);

        $this->load->view($this->header);
        $this->load->view('produtos/produtos_visualizar', $this->data);
        $this->load->view($this->footer);
    }
    
    
}
