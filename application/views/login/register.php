<?php 
    echo form_open("auth/registrar", array('class'=> 'col-md-6 offset-md-3'));
    echo form_fieldset('Cadastro de Usuarios', array('class'=>'text-primary'));
        echo form_label("Nome", "nome");
            echo form_input(array(
                "name" => "nome",
                "id" => "nome",
                "value" =>  set_value('nome'),
                "class" => "form-control",
                "maxlength" => "255"
            ));
            echo form_error('nome', '<div class="text-danger">*','</div>');

            echo form_label("Email", "email");
            echo form_input(array(
                "name" => "email",
                "id" => "email",
                "value" =>  set_value('email'),
                "class" => "form-control",
                "maxlength" => "255"
            ));
            echo form_error('email', '<div class="text-danger">*','</div>');

            echo form_label("Senha", "senha");
            echo form_input(array(
                "name" => "senha",
                "id" => "senha",
                "value" =>  set_value('senha'),
                "class" => "form-control",
                "maxlength" => "255"
            ));
            echo form_error('senha', '<div class="text-danger">*','</div>');
            
            echo anchor(base_url("auth"), 'Cancelar', array('class' => 'btn btn-secondary mt-2'));

            echo form_button(array(
                "class" => "btn btn-primary mt-2 ml-2",
                "content" => "Cadastrar",
                "type" => "submit"
            ));
            
        echo form_fieldset_close();
    echo form_close() 
?>