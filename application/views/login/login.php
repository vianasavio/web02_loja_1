<?php 
    echo form_open("auth/autenticar", array('class'=> 'col-md-6 offset-md-3'));
    echo form_fieldset('Login', array('class'=>'text-primary'));

            echo form_label("Email", "email");
            echo form_input(array(
                "type" => "email",
                "name" => "email",
                "id" => "email",
                "class" => "form-control",
                "maxlength" => "255",
                "required" => "true"
            ));

            echo form_label("Senha", "senha");
            echo form_input(array(
                "type" => "password",
                "name" => "senha",
                "id" => "senha",
                "class" => "form-control",
                "maxlength" => "255",
                "required" => "true"
            ));

            
            echo anchor(base_url('auth/registrar'), 'Registre-se', array('class'=>'btn btn-secondary mt-2'));

            echo form_button(array(
                "class" => "btn btn-primary mt-2 ml-2",
                "content" => "Entrar",
                "type" => "submit"
            ));
            
        echo form_fieldset_close();
    echo form_close();
?>
