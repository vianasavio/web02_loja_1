<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Produtos</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="<?= base_url("assets/bootstrap-4.1.3/css/bootstrap.min.css") ?>" />
    
    <style>

        h1{
            color: #0080ff;
        }

        body {
            background: #f2f2f2;
        }

        .bg_table_thead{
            background: #0080ff;
            color: aliceblue;
        }

        .bg_body_table{
            background-color: #cce6ff;
        }

        .bg_body_table:hover{
            background-color: #80c1ff;
        }

    </style>
</head>
<body>
    <div class="content container mt-5">
    <?php showMessage() ?>