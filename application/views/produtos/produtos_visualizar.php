
<h1 class="text-center"><?= $produto->nome ?></h1>

<div class="table-responsive">
    <table class="table">
    <thead class="bg_table_thead">
    <tr>
        <th scope="col">Nome</th>
        <th scope="col">Descrição</th>
        <th scope="col">Preço</th>
        <th scope="col">Usuario</th>
    </tr>
    </thead>
    <tbody>
    
        <tr  class="bg_body_table" onclick='location.href = "<?= base_url("produtos/visualizar/{$produto->id}") ?>"' >
            <td><?= $produto->nome ?></td>
            <td><?= $produto->descricao ?></td>
            <td><?= numerosEmReais($produto->preco) ?></td>
            <td><?= $user->nome ?></td>
        </tr>
        
    </tbody>
    </table>
</div>


<?php
    echo anchor(base_url("produtos"), 'Voltar', array('class' => 'btn btn-secondary mt-2'));
    echo anchor(base_url("produtos/editar/{$produto->id}"), 'Editar', array('class' => 'btn btn-primary mt-2 ml-2'));

    

?>