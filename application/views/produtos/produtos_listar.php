
<h1 class="text-center">Lista de Produtos</h1>

<div class="table-responsive">
    <table class="table">
    <thead class="bg_table_thead">
    <tr>
        <th scope="col">#</th>
        <th scope="col">Nome</th>
        <th scope="col">Descrição</th>
        <th scope="col">Preço</th>
        <th scope="col">Usuario</th>
    </tr>
    </thead>
    <tbody>
        <?php foreach ($produtos as $key => $produto): ?>
        
        <tr  class="bg_body_table" onclick='location.href = "<?= base_url("produtos/visualizar/{$produto->id}") ?>"' >
            <th scope="row"><?= $key + 1  ?></th>
            <td><?= $produto->nome ?></td>
            <td><?= $produto->descricao ?></td>
            <td><?= numerosEmReais($produto->preco) ?></td>
            <td><?= $this->usuarios_model->getUser($produto->usuario_id)->nome ?></td>
        </tr>
        
        <?php endforeach ?>
    </tbody>
    </table>
</div>


<?php

    echo anchor(base_url("produtos/cadastrar"), 'Novo Produto', array('class' => 'btn btn-primary mt-2'));

    echo anchor(base_url("auth/logout"), 'Logout', array('class' => 'btn btn-danger mt-2 ml-2'));

?>