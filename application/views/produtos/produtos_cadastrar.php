
<?php 
    echo form_open("produtos/cadastrar");
    echo form_fieldset('Cadastro de Produtos', array('class'=>'text-primary'));
        echo form_label("Nome", "nome");
            echo form_input(array(
                "name" => "nome",
                "id" => "nome",
                "value" =>  set_value('nome'),
                "class" => "form-control",
                "maxlength" => "255"
            ));
            echo form_error('nome', '<div class="text-danger">*','</div>');

            echo form_label("Descrição", "descrição");
            echo form_input(array(
                "name" => "descricao",
                "id" => "descricao",
                "descricao" => set_value('descricao'),
                "class" => "form-control",
                "maxlength" => "255"
            ));
            echo form_error('descricao', '<div class="text-danger">*','</div>');

            echo form_label("Preço", "preco");
            echo form_input(array(
                "name" => "preco",
                "id" => "preco",
                "preco" => set_value('preco'),
                "class" => "form-control",
                "maxlength" => "255"
            ));
            echo form_error('preco', '<div class="text-danger">*','</div>');
           
            echo form_button(array(
                "class" => "btn btn-success mt-2",
                "content" => "Salvar",
                "type" => "submit"
            ));

            echo anchor(base_url("produtos"), 'Cancelar', array('class' => 'btn btn-secondary mt-2'));

        echo form_fieldset_close();

    echo form_close() 
?>
