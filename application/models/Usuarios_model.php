<?php 

class Usuarios_model extends CI_Model{

    public function salvarUsuario($usuarios){
        
        return $this->db->insert('usuarios', $usuarios);
    }

    public function getUser($id){
    
        $this->db->where('id', $id);
        return current($this->db->get('usuarios')->result());
    }

    public function buscarUsuario($email, $senha){
    
        $this->db->where('email', $email);
        $this->db->where('senha', $senha);
        return current($this->db->get('usuarios')->result());
    }
    

}