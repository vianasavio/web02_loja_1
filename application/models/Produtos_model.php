<?php 

class Produtos_model extends CI_Model{

    public function buscaProdutos(){
        
        return $this->db->get('produtos')->result();
    }

    public function getProduto($id){
        $this->db->where('id', $id);
        return current($this->db->get('produtos')->result());
    }

    public function insert($data){
        return $this->db->set($data)->insert('produtos');
    }

}