<?php

function redirectError($url, $msg) {
    get_instance()->session->set_flashdata("type", 'danger');
    get_instance()->session->set_flashdata("message", $msg);
    redirect($url);
}

function redirectSuccess($url, $msg) {
    get_instance()->session->set_flashdata("type", 'success');
    get_instance()->session->set_flashdata("message", $msg);
    redirect($url);
}

function showMessage() {
    
    $message = get_instance()->session->flashdata('message');
    $type = get_instance()->session->flashdata('type');
    
    if(get_instance()->session->flashdata('message') && get_instance()->session->flashdata('type') ){
        
        echo "<div class='mt-2 mb-2 alert alert-{$type} alert-dismissible fade show' role='alert'>
        {$message}
        <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
          <span aria-hidden='true'>&times;</span>
        </button>
      </div>";
    }
}
